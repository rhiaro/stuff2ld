from stuff2ld import app
from flask import Flask, render_template, url_for
from flask.ext.uploads import init, save, Upload
from blogger2ld import ttl_from_file

@app.errorhandler(404)
def page_not_found(e):
	return render_template('404.html'), 404

#############################################
# Home, web and intro and stuff.
#############################################
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/web')
def web():
	return render_template('webui.html')

#############################################
# Blogger conversion stuff
#############################################
@app.route('/blogger2ld')
def blogger():
	file_in = get_input_file()
	results = "blah"
	return render_template('blogger.html', 
							turtle=results, 
							filein=file_in)

def get_input_file():
	return "stuff2ld/static/blog.xml"

def xml_to_ttl(file_in):
	return ttl_from_file(file_in)

#############################################
# Twitter conversion stuff
#############################################
@app.route('/twitter2ld')
def twitter():
	return render_template('twitter.html')
