from bs4 import BeautifulSoup
from rdflib import Graph, Literal, BNode, Namespace, RDF, URIRef
from rdflib.namespace import FOAF, DC, DCTERMS, XSD

def soup_from_file(file):
	handler = open(file).read()
	soup = BeautifulSoup(handler)
	return soup

def bind_namespaces(graph):
	global SIOC
	global BLOG # Temporary
	SIOC = Namespace("http://rdfs.org/sioc/types#")
	BLOG = Namespace("http://rhiaro.co.uk/vocab/blog#")

	graph.bind("foaf", FOAF)
	graph.bind("rdf", RDF)
	graph.bind("dc", DC)
	graph.bind("dct", DCTERMS)
	graph.bind("blog", BLOG)
	graph.bind("sioc", SIOC)

def merge_graphs(graphs):
	# Accepts a list of individual post graphs to make a full blog graph
	biggraph = Graph()
	for graph in graphs:
		biggraph = biggraph + graph
	return biggraph

def make_blog_graph(soup):
	posts = []
	graphs = []
	for entry in soup.findAll('entry'):
		posts.append(Post(entry, "http://rhiaro.co.uk/"))

	for post in posts:
		graphs.append(post.graph)

	final = merge_graphs(graphs)
	ttl = final.serialize(format='turtle')

	return ttl

def ttl_from_file(file):
	soup = soup_from_file(file)
	return unicode(make_blog_graph(soup), "utf8")

class Post(object):

	def __init__(self, entry, uri_base):
		# As assigned by blogger, used here for convenience
		self.raw_xml = entry
		self.uri_base = uri_base
		self.bloggerid = self.set_bloggerid()
		self.pubdate = self.set_pubdate()
		self.moddate = self.set_moddate()
		self.title = self.set_title()
		self.content = self.set_content()
		self.creator = self.set_creator()
		self.type = self.set_type()
		self.tags = self.set_tags()
		self.uri = self.make_uri()
		self.graph = self.make_graph()

	def set_bloggerid(self):
		return self.raw_xml.id.text

	def set_pubdate(self):
		# TODO: deal with pubdates for drafts
		return self.raw_xml.published.text

	def set_moddate(self):
		return self.raw_xml.updated.text

	def set_title(self):
		return self.raw_xml.title.text

	def set_content(self):
		return self.raw_xml.content.text

	def set_creator(self):
		return self.raw_xml.author.uri.text

	def set_type(self):
		cat_uri = self.get_all_categories("post")
		# Should only ever be one result. If there are more, take only the first..
		post_cat = cat_uri[0].partition("#")
		if post_cat[2] == "comment":
			return "Comment"
		elif post_cat[2] == "post":
			return "BlogPost"
		else:
			pass # Ignore it, blog pages

	def set_tags(self):
		return self.get_all_categories("tag")

	def make_uri(self):
		date = self.pubdate.partition("T")[0]
		title = self.title.lower().replace(" ","-")
		slug = ''.join([c for c in title if c.isalnum() or c == '-'])[:16]
		return "%s%s/%s-%s" % (self.uri_base, self.type, date, slug)


	def make_graph(self):
		g = Graph()
		bind_namespaces(g)
		this = URIRef(self.uri)
		# Types
		if self.type == "Comment":
			g.add( (this, RDF.type, SIOC.Comment) )
		elif self.type == "BlogPost":
			g.add( (this, RDF.type, SIOC.BlogPost) )
		# Dates
		if self.pubdate:
			pdate = Literal(self.pubdate, datatype=XSD.datetime)
			g.add( (this, DCTERMS.created, pdate) )
		if self.moddate:
			mdate = Literal(self.moddate, datatype=XSD.datetime)
			g.add( (this, DCTERMS.modified, mdate) )
		# Text
		g.add( (this, DCTERMS.title, Literal(self.title)) )
		g.add( (this, SIOC.content, Literal(self.content)) )
		# Creator
		# Just use blogger's URIs for now. Can connect to other personal URIs with sameAs for people who care.
		g.add((this, DC.creator, URIRef(self.creator)))
		# Tags, lists
		for tag in self.tags:
			g.add((this, SIOC.topic, Literal(tag)))
		# Connections, eg. sioc:reply_of for Comments.
		# Using bnodes for now as all I can pull out of the XML is the blogger ids. Can infer actual URIs from final graph.
		try:
			replyid = self.raw_xml.find('thr:in-reply-to').attrs['ref']
			bnode = BNode()
			g.add ( (bnode, BLOG.bloggerid, Literal(replyid)) )
			g.add( (this, SIOC.reply_of, bnode) )
		except AttributeError: # is not a reply
			pass

		return g

	def get_all_categories(self, cat_type):
		categories = []
		if cat_type == "tag":
			# Labels
			scheme = "http://www.blogger.com/atom/ns#"
		if cat_type == "post":
			# Blogger post type descriptors
			scheme = "http://schemas.google.com/g/2005#kind"
		cats = self.raw_xml.findAll('category')
		for cat in cats:
			cat_attrs = dict(cat.attrs)
			if cat_attrs['scheme'] == scheme:
				categories.append(cat_attrs['term'])
		return categories